package main

import (
	"./config"
	"./entities"
	"./models"
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"gopkg.in/mgo.v2/bson"
	"log"
	"net/http"
)

func main ()  {

	//init Router
	router := mux.NewRouter()

	//Router handlers /endpoints
	router.HandleFunc("/api/products",getProducts).Methods("GET")
	router.HandleFunc("/api/product/{id}",getProduct).Methods("GET")
	router.HandleFunc("/api/product",newProduct).Methods("POST")
	router.HandleFunc("/api/productName",updateProductName).Methods("PUT")
	router.HandleFunc("/api/product/{id}",deleteProduct).Methods("DELETE")
	log.Fatal(http.ListenAndServe(":8000",router))
}

//get list of products
func getProducts(w http.ResponseWriter, r *http.Request){
	fmt.Println("Get Product List")
	db,err := config.GetMongoDB()
	if err != nil {
		respondWithError(w,http.StatusBadRequest,err.Error())
		return
	} else {
		productModel := models.ProductModel{
			Db:         db,
			Collection: "product",
		}
		products, err2 := productModel.FindAll()
		if err2 !=nil{
			respondWithError(w,http.StatusBadRequest,err2.Error())
		} else {
			respondWithJson(w,http.StatusOK,products)
		}
	}
}

//get a product by id
func getProduct(w http.ResponseWriter, r *http.Request){
	fmt.Println("Get Product Item")
	db,err := config.GetMongoDB()
	if err != nil {
		respondWithError(w,http.StatusBadRequest,err.Error())
		return
	} else {
		productModel := models.ProductModel{
			Db:         db,
			Collection: "product",
		}
		vars :=mux.Vars(r)
		id:=vars["id"]
		product, err2 := productModel.Find(id)
		if err2 !=nil{
			respondWithError(w,http.StatusBadRequest,err2.Error())
		} else {

			respondWithJson(w,http.StatusOK,product)
		}
	}
}

//insert new product
func newProduct(w http.ResponseWriter, r *http.Request){
	fmt.Println("New Product")
	db,err := config.GetMongoDB()
	if err != nil {
		respondWithError(w,http.StatusBadRequest,err.Error())
		return
	} else {
		productModel := models.ProductModel{
			Db:         db,
			Collection: "product",
		}
		var product entities.Product
		product.Id =bson.NewObjectId()
		err := json.NewDecoder(r.Body).Decode(&product)
		if err != nil {
			respondWithError(w,http.StatusBadRequest,err.Error())
			return
		} else {
			err3 :=productModel.Create(&product)
			if err3 != nil {
				respondWithError(w,http.StatusBadRequest,err3.Error())
				return
			} else {
				respondWithJson(w,http.StatusOK,product)
			}
		}
	}
}

//Update Product
func updateProductName(w http.ResponseWriter, r *http.Request){
	fmt.Println("Update Product")
	db,err := config.GetMongoDB()
	if err != nil {
		respondWithError(w,http.StatusBadRequest,err.Error())
		return
	} else {
		productModel := models.ProductModel{
			Db:         db,
			Collection: "product",
		}
		var product entities.Product
		err := json.NewDecoder(r.Body).Decode(&product)
		if err != nil {
			respondWithError(w,http.StatusBadRequest,err.Error())
			return
		} else {
			err3 :=productModel.Update(&product)
			if err3 != nil {
				respondWithError(w,http.StatusBadRequest,err3.Error())
				return
			} else {
				respondWithJson(w,http.StatusOK,product)
			}
		}
	}
}

//Delete Product
func deleteProduct(w http.ResponseWriter, r *http.Request){
	fmt.Println("Delete Product")
	db,err := config.GetMongoDB()
	if err != nil {
		respondWithError(w,http.StatusBadRequest,err.Error())
		return
	} else {
		productModel := models.ProductModel{
			Db:         db,
			Collection: "product",
		}
		vars :=mux.Vars(r)
		id:=vars["id"]
		product,_:=productModel.Find(id)
		err2 := productModel.Delete(product)
		if err2 !=nil{
			respondWithError(w,http.StatusBadRequest,err2.Error())
		} else {
			respondWithJson(w,http.StatusOK,product)
		}
	}
}

func respondWithError(writer http.ResponseWriter, code int, msg string) {
	respondWithJson(writer,code, map[string]string{"error":msg})
}

func respondWithJson(writer http.ResponseWriter, code int, payload interface{}) {
	response,_:= json.Marshal(payload)
	writer.Header().Set("Content-type", "application/json")
	writer.WriteHeader(code)
	writer.Write(response)
}