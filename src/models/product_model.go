package models

import (
	"gopkg.in/mgo.v2"
	"../entities"
	"gopkg.in/mgo.v2/bson"
)

type ProductModel struct {
	Db *mgo.Database
	Collection string
}

func (productModel ProductModel) FindAll() (products []entities.Product,err error) {
	err = productModel.Db.C(productModel.Collection).Find(bson.M{}).All(&products)
	return
}

func (productModel ProductModel) Find(id string) (product entities.Product,err error) {
	err = productModel.Db.C(productModel.Collection).FindId(bson.ObjectIdHex(id)).One(&product)
	return
}

func (productModel ProductModel) Create(product *entities.Product) error {
	err := productModel.Db.C(productModel.Collection).Insert(&product)
	return err
}

func (productModel ProductModel) Update(product *entities.Product) error {
	err := productModel.Db.C(productModel.Collection).UpdateId(product.Id, &product)
	return err
}

func (productModel ProductModel) Delete(product entities.Product) error {
	err := productModel.Db.C(productModel.Collection).Remove(product)
	return err
}